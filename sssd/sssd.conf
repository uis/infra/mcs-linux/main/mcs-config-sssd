# This is the main MCS Linux SSSD configuration file.
# It should be installed in /etc/sssd/sssd.conf,
# owned by root.root, and be set mode 600.

[sssd]
config_file_version = 2
services = nss, pam
domains = PWFAD.PWF.PRIVATE.CAM.AC.UK
debug_level = 3
debug_timestamps = false

[nss]
override_homedir = /home/%u
override_shell = /bin/bash
debug_level = 3
debug_timestamps = false

[pam]
pam_verbosity = 2
debug_level = 3
debug_timestamps = false

[domain/PWFAD.PWF.PRIVATE.CAM.AC.UK]
debug_level = 3
debug_timestamps = false

id_provider = ad
auth_provider = ad
chpass_provider = ad

ad_domain = pwfad.pwf.private.cam.ac.uk

# The names of users is held in the displayName field in the AD.
ldap_user_gecos = displayName

# Disable AD machine account age checking
ad_maximum_machine_account_password_age = 0
dyndns_update = false

# Do not attempt to validate acquired kerberos tickets by acquiring
# new TGTs for the machine account.
krb5_validate = false

# The AD does not contain useful UID/GID values for most objects, only users.
# Thus, each machine should use locally-defined IDs, and LDAP queries should key on SIDs.
ldap_id_mapping = true
ldap_idmap_range_min  =    2000000
ldap_idmap_range_max  = 2002000000
ldap_idmap_range_size =   20000000

# Active Directory does not enforce case-sensitivity
case_sensitive = false

# Use the unprivileged 'linuxldap' account to query user data from the AD.
ldap_sasl_authid = linuxldap@PWFAD.PWF.PRIVATE.CAM.AC.UK
krb5_keytab = /etc/sssd/sssd.keytab

# Certificate validation is mandatory.
ldap_tls_reqcert = demand

# Always use and display realm names in upper-case.
ldap_force_upper_case_realm = true

# Automatically renew user Kerberos credentials every 30 minutes.
krb5_renew_interval = 30m
